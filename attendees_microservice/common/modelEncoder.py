from json import JSONEncoder
from attendees.models import Attendee, ConferenceVO
from django.db.models import QuerySet
from datetime import datetime


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):  # the takes
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()  # Iso is for changing date time formats
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(
            o, self.model
        ):  # If the 0 is an object of the model passed in
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            # if o has the attribute get_api_url
            #    then add its return value to the dictionary
            #    with the key "href"
            for property in self.properties:
                value = getattr(
                    o, property
                )  # getattribute function, object first then property
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)  # .default
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)
        #   if the object to decode is the same class as what's in the
        #   model property, then
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
        #     * for each name in the properties list
        #         * get the value of that property from the model instance
        #           given just the property name
        #         * put it into the dictionary with that property name as
        #           the key
        #     * return the dictionary
        #   otherwise,
        #       return super().default(o)  # From the documentation

    def get_extra_data(self, o):
        return {}


# class LocationListEncoder(ModelEncoder):
#     model = Location
#     properties = [
#         "name",
#         "city",
#     ]


# class LocationDetailEncoder(ModelEncoder):
#     model = Location
#     properties = ["name", "city", "room_count", "created", "updated"]

#     def get_extra_data(self, o):
#         return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name"]


# class ConferenceDetailEncoder(ModelEncoder):
#     model = Conference
#     properties = [
#         "name",
#         "description",
#         "max_presentations",
#         "max_attendees",
#         "starts",
#         "ends",
#         "created",
#         "updated",
#         "location",
#     ]  # what are the parts of the conference that you want to convert to JSON?
#     encoders = {
#         "location": LocationListEncoder(),
#     }


# class PresentationDetailEncoder(ModelEncoder):
#     model = Presentation
#     properties = [
#         "presenter_name",
#         "company_name",
#         "presenter_email",
#         "title",
#         "synopsis",
#         "created",
#     ]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "company_name",
        "conerence",
    ]
