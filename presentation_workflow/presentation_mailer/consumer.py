import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


# def process_message(ch, method, properties, body):
#     print(
#         "  Received %r" % body
# )  # %r changes an object into a readable string


def process_approval(ch, method, properties, body):
    content = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f'content["presenter_name"], we are happy to tell you that your content["title"] has been accepted',
        "admin@conference.go",
        [content["presenter_email"]],
        fail_silently=False,
    )
    print("approval email sent")


def process_rejection(ch, method, properties, body):
    content = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f'content["presenter_name"], we are happy to tell you that your content["title"] has been accepted',
        "admin@conference.go",
        [content["presenter_email"]],
        fail_silently=False,
    )
    print("rejection email sent")


while True:

    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=process_approval,
        auto_ack=True,
    )
    channel.queue_declare(queue="presentation_rejections")
    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=process_rejection,
        auto_ack=True,
    )
    channel.start_consuming()
