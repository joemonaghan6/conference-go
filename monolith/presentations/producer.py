import pika
import json


def approved_and_rejected_presentation(
    presenter_name, presenter_email, title, queue
):
    parameters = pika.ConnectionParameters(
        host="rabbitmq"
    )  # Getting setup to create a rabbitmq ontainer
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(
        queue=queue
    )  # two different queues approved and rejected which ae named based on the views
    channel.basic_publish(
        exchange="",
        routing_key=queue,
        body=json.dumps(
            {
                "presenter_name": presenter_name,
                "presenter_email": presenter_email,
                "title": title,
            }
        ),
    )

    connection.close()
